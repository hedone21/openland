/*
 * Copyright (c) 2019 Minyoung.Go <hedone21@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include "../client/launcher.h"

#define OL_TEST_LAUNCHER_SOCKET "/tmp/ol_test_launcher_socket"

bool launcher_launch_weston_termianl()
{
	struct sockaddr_un server, client;
	unsigned int client_len;
	int sock;
	char buf[1024];

	bool running = true;
	bool ret = false;

	sock = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sock < 0) {
		printf("ERROR OPENING SOCKET PAIR = %d\n", sock);
		return ret;
	}

	bzero(&server, sizeof(server));
	server.sun_family = AF_UNIX;
	strcpy(server.sun_path, OL_TEST_LAUNCHER_SOCKET);
	if (bind(sock, (struct sockaddr*)&server, sizeof(struct sockaddr_un)) < 0) {
		perror("binding stream socket");
		return ret;
	}
	printf("Socket has name %s\n", server.sun_path);
	listen(sock, 1);
	client_len = sizeof(client);

	ret = ol_launcher_launch("tests/launcher_client-test", "a", "bc", "def", NULL);

	while (running) {
		int msg_len = recvfrom(sock, (void *)&buf, sizeof(buf), 0, (struct sockaddr *)&client, &client_len); 
		if (msg_len > 0) {
			running = false;
		}
	}

	close(sock);

	return (strcmp(buf, "tests/launcher_client-test a bc def ") == 0);
}

int main(int argc, char *argv[])
{
	int number_failed = 0;

	number_failed += !launcher_launch_weston_termianl();

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
