/* * Copyright (c) 2019 Minyoung.Go <hedone21@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include "../client/desktop.h"

static struct ol_desktop *desktop;

bool wb_connect()
{
	struct wl_display *display;
	bool ret = false;
	desktop = ol_desktop_init(); 
	if (!desktop) 
		goto out;

	display = wl_display_connect(NULL);
	if (!display)
		goto out;

	ret = ol_desktop_connect_wayland(desktop, display);

out:
	return ret;
}

int wb_check_components()
{
	int ret = 0;

	desktop->compositor ? 0 : ret++;
	desktop->output ? 0 : ret++;
	desktop->seat ? 0 : ret++;
	desktop->shm ? 0 : ret++;

	return ret;
}


int main(int argc, char *argv[])
{
	int number_failed = 0;

	number_failed += !wb_connect();
	number_failed += wb_check_components();

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
