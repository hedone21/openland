/*
 * Copyright (c) 2019 Minyoung.Go <hedone21@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#define OL_TEST_LAUNCHER_SOCKET "/tmp/ol_test_launcher_socket"

int main(int argc, char *argv[])
{
	struct sockaddr_un server;
	char arg[1024] = {0, };
	int sockets[2];
	int sock;
	int ret = 0;

	for (int i = 0; i < argc; i++) {
		strcat(arg, argv[i]);
		strcat(arg, " ");
	}

	printf("%s\n", arg);

	sleep(1);

	sock = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sock < 0) {
		printf("ERROR OPENING SOCKET PAIR = %d\n", sock);
		return ret;
	}

	bzero(&server, sizeof(server));
	server.sun_family = AF_UNIX;
	strcpy(server.sun_path, OL_TEST_LAUNCHER_SOCKET);

	if (sendto(sock, (void *)&arg, sizeof(arg), 0,(struct sockaddr *)&server, sizeof(server)) < 0) {
		perror("send error : ");
		exit(0);
	}

	printf("%s\n", arg);

	close(sock);

	return 0;
}
