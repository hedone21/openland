/*
 * Copyright (c) 2019 Minyoung.Go <hedone21@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <unistd.h>
#include <sys/types.h>
#include "protocol/weston-desktop-shell-client-protocol.h"
#include "registry.h"
#include "desktop.h"
#include "surface.h"
#include "log.h"

static void weston_desktop_shell_configure(void *data, 
		struct weston_desktop_shell *weston_desktop_shell,
		uint32_t edges, struct wl_surface *surface,
		int32_t width, int32_t height)
{
	struct ol_desktop *desktop;
	desktop = (struct ol_desktop*)data;
	openland_dbg("Set size request w=%d h=%d\n", width, height);
	gtk_widget_set_size_request (desktop->background->window,
			width, height);

	weston_desktop_shell_desktop_ready (desktop->wshell);

}

static void weston_desktop_shell_prepare_lock_surface(void *data,
		struct weston_desktop_shell *weston_desktop_shell)
{
}

static void weston_desktop_shell_grab_cursor(void *data,
		struct weston_desktop_shell *weston_desktop_shell, 
		uint32_t cursor)
{
}

static const struct weston_desktop_shell_listener wshell_listener = {
	weston_desktop_shell_configure,
	weston_desktop_shell_prepare_lock_surface,
	weston_desktop_shell_grab_cursor
};

static void shm_format(void *data, struct wl_shm *wl_shm, uint32_t format)
{
	/*
	   struct openland_display *display = data;

	   if (format == WL_SHM_FORMAT_RGB565)
	   display->has_rgb565 = 1;
	   */
}

struct wl_shm_listener shm_listener = {
	shm_format
};

static void registry_handle_global(void *data, struct wl_registry *registry, 
		uint32_t id, const char *interface, uint32_t version)
{
	struct ol_desktop *desktop = (struct ol_desktop*)data;
	openland_dbg("Got a registry event for %s id %d\n", interface, id);
	if (!strcmp (interface, "weston_desktop_shell"))
	{
		desktop->wshell = wl_registry_bind(registry, id,
				&weston_desktop_shell_interface, 1);
		weston_desktop_shell_add_listener(desktop->wshell, 
				&wshell_listener, desktop);
	}else if (strcmp(interface, "wl_compositor") == 0){
		desktop->compositor = wl_registry_bind(registry, id,
				&wl_compositor_interface, 3);
	}else if (strcmp(interface, "wl_output") == 0) {
		desktop->output = wl_registry_bind(registry, id, 
				&wl_output_interface, 2);
	}else if (strcmp(interface, "wl_seat") == 0) {
		desktop->seat = wl_registry_bind(registry, id, &wl_seat_interface, 5);
	} else if (strcmp(interface, "wl_shm") == 0) {
		desktop->shm = wl_registry_bind(registry, id, &wl_shm_interface, 1);
	}
}

static void registry_remove_global(void *data, struct wl_registry *registry, 
		uint32_t id)
{
	openland_dbg("Got a registry losing event for %d\n", id);
}

const struct wl_registry_listener registry_listener = {
	registry_handle_global,
	registry_remove_global
};
