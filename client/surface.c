/*
 * Copyright (c) 2019 Minyoung.Go <hedone21@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdbool.h>
#include "protocol/weston-desktop-shell-client-protocol.h"
#include "surface.h"
#include "image.h"
#include "log.h"

bool ol_surface_create_background(struct ol_desktop *desktop)
{
	struct ol_surface *background;
	GdkWindow *gdk_window;

	background = calloc(1, sizeof(struct ol_surface));
	if (background == NULL) {
		openland_err("oom error\n");
		return -1;
	}

	background->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	gtk_window_set_title(GTK_WINDOW(background->window), "openland");
	gtk_window_set_decorated(GTK_WINDOW(background->window), FALSE);
	ol_image_open(background, "share/openland.png");
	ol_image_scale_fullscreen(background);
	// gtk_window_fullscreen(GTK_WINDOW(background->window));
	ol_image_draw(background);
	gtk_widget_realize(background->window);

	gdk_window = gtk_widget_get_window(background->window);
	gdk_wayland_window_set_use_custom_surface(gdk_window);

	background->surface = gdk_wayland_window_get_wl_surface(gdk_window);
	//weston_desktop_shell_set_user_data(desktop->wshell, desktop);
	//weston_desktop_shell_set_background(desktop->wshell, desktop->output,
	//		background->surface);

	desktop->background = background;

	gtk_widget_show_all(background->window);

	return 0;
}
