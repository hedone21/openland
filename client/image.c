/*
 * Copyright (c) 2019 Minyoung.Go <hedone21@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <gtk/gtk.h>
#include "image.h"
#include "log.h"

void ol_image_open(struct ol_surface *surface, const char *path)
{
	char abs_path[PATH_MAX] = {'\0', };
	getcwd(abs_path, PATH_MAX);
	strcat(abs_path, "/");
	strncat(abs_path, path, PATH_MAX);
	openland_dbg("[%d] %s - %s\n", PATH_MAX, path, abs_path);

	surface->pixbuf = gdk_pixbuf_new_from_file(abs_path, NULL);
}

void ol_image_scale_fullscreen(struct ol_surface *surface)
{
	GdkScreen *screen = gdk_screen_get_default ();
	gint screen_width, screen_height;
	gint original_width, original_height;
	gint final_width, final_height;
	gdouble ratio_horizontal, ratio_vertical, ratio;
	GdkPixbuf *unscaled_image;

	unscaled_image = surface->pixbuf;

	screen_width = gdk_screen_get_width(screen);
	screen_height = gdk_screen_get_height(screen);
	original_width = gdk_pixbuf_get_width(unscaled_image);
	original_height = gdk_pixbuf_get_height(unscaled_image);

	ratio_horizontal = (double)screen_width / original_width;
	ratio_vertical = (double)screen_height / original_height;
	ratio = MAX (ratio_horizontal, ratio_vertical);

	final_width = ceil(ratio * original_width);
	final_height = ceil(ratio * original_height);

	surface->pixbuf = gdk_pixbuf_scale_simple(unscaled_image, final_width, final_height, GDK_INTERP_BILINEAR);
	g_object_unref(unscaled_image);
}

static gboolean draw_cb(GtkWidget *widget, cairo_t *cr, gpointer data)
{
	struct ol_surface *surface = data;

	gdk_cairo_set_source_pixbuf(cr, surface->pixbuf, 0, 0);
	cairo_paint(cr);

	return TRUE;
}

void ol_image_draw(struct ol_surface *surface)
{
	g_signal_connect(surface->window, "draw", G_CALLBACK(draw_cb), surface);
}
