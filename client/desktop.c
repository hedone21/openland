/*
 * Copyright (c) 2019 Minyoung.Go <hedone21@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>
#include <stdlib.h>
#include "protocol/weston-desktop-shell-client-protocol.h"
#include "desktop.h"
#include "registry.h"
#include "log.h"

struct ol_desktop* ol_desktop_init()
{
	struct ol_desktop *desktop;

	desktop = (struct ol_desktop*)malloc(sizeof(*desktop));
	if (desktop == NULL) {
		goto OUT;
	}

	memset(desktop, 0, sizeof(*desktop));

OUT:
	return desktop;
}

bool ol_desktop_connect_wayland(struct ol_desktop *desktop, struct wl_display *display)
{
	bool ret = FALSE;
	int status = -1;

	desktop->display = display;
	if (desktop->display == NULL) {
		goto OUT;
	}

	desktop->registry = wl_display_get_registry(desktop->display);
	if (desktop->registry == NULL) {
		goto OUT;
	}

	wl_registry_add_listener(desktop->registry, &registry_listener, desktop);

	status = wl_display_dispatch(desktop->display);
	if (status == -1) {
		goto OUT;
	}

	status = wl_display_roundtrip(desktop->display);
	if (status == -1) {
		goto OUT;
	}

	ret = TRUE;

OUT:
	return ret;
}
