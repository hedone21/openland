/*
 * Copyright (c) 2019 Minyoung.Go <hedone21@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>

#include "launcher.h"
#include "log.h"

/* end of arg MUST be NULL */
bool ol_launcher_launch(char *argv0, ...)
{
	int pid;
	int count = 0;
	char *argv;
	char **argv_list;
	va_list ap;

	va_start(ap, argv0);
	do {
		argv = va_arg(ap, char*);
		count++;
	}while (argv != NULL);
	va_end(ap);

	argv_list = (char**)malloc(count * sizeof(char*));
	if (argv_list == NULL) {
		openland_err("OOM error\n");
		return false;
	}

	argv_list[0] = argv0;
	openland_dbg("argv[0] %s\n", argv0);

	count = 1;
	va_start(ap, argv0);
	do {
		argv = va_arg(ap, char*);
		openland_dbg("argv[%d] %s\n", count, argv);
		argv_list[count++] = argv;
	}while (argv != NULL);
	va_end(ap);

	pid = fork();

	if (pid < 0) {
		return false;
	}else if (pid == 0) {
		int ret;
		openland_dbg("launch %s\n", argv0);
		ret = execv(argv0, argv_list);
		openland_err("exec failed. MUST not be reached here (%d)\n", ret);
		exit(EXIT_FAILURE);
	}

	free(argv_list);

	return true;
}
