# openland
This program is a weston plugin to replace openbox.

## NOTICE
- This program is an early development version and does not guarantee stability.
- Lots of the source code for this program has been produced with reference 
to weston.

## Build & run
preinstall packages
~~~
sudo pacman -S wayland weston meson
~~~

build
~~~
meson build
ninja -C build
~~~

run
~~~
./run.sh
~~~

## License
MIT License
