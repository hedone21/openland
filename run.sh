#!/bin/bash

WESTON_CONFIG=$PWD"/config.txt"

echo "[shell]" > $WESTON_CONFIG
echo "client="$PWD"/build/client/openland" >> $WESTON_CONFIG

export OPENLAND_CLIENT=$PWD"/build/client/openland"
weston -c $WESTON_CONFIG --shell=/home/go/Git/openland/build/shell/openland-shell.so
